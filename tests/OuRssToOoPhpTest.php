<?php
require_once (__DIR__ . '/../src/OuRssToOoPhp.php');

use OuRssToOoPhp\OuRssToOoPhp;
use PHPUnit\Framework\TestCase;

class OuRssToOoPhpTest extends TestCase
{
	public function testBaseline()
	{
		$jason = new OuRssToOoPhp('https://bluedragon-films.com/category/sprocket-damage/feed/');
		$xml = $jason->testEchoDoc();
		$this->assertStringContainsString('<?xml version', $xml);
	}

	public function testGetPodcastTitle()
	{
		$expected = 'Sprocket Damage';
		$jason = new OuRssToOoPhp('https://bluedragon-films.com/category/sprocket-damage/feed/');
		$podcast = $jason->getPodcast();
		$actual = $podcast->title;
		$this->assertEquals($expected, $actual);
	}

	public function testGetPodcastCategories()
	{
		$jason = new OuRssToOoPhp('https://bluedragon-films.com/category/sprocket-damage/feed/');
		$podcast = $jason->getPodcast();
		$categories = $podcast->categories;
		$this->assertGreaterThan(0, count($categories));
		$this->assertContains('Arts', $categories);
	}

	public function testGetEpisodes()
	{
		$jason = new OuRssToOoPhp('https://bluedragon-films.com/category/sprocket-damage/feed/');
		$episodes = $jason->getEpisodes();
		$this->assertGreaterThan(0, count($episodes));

		$lastEpisode = $episodes[count($episodes) - 1];
		$this->assertNotEmpty($lastEpisode->enclosure_length);
	}

	public function testGetOptionalNode_DoesNotThrow()
	{
		$jason = new OuRssToOoPhp('https://bluedragon-films.com/category/sprocket-damage/feed/');
		$episodes = $jason->getEpisodes();
		$this->assertNull($episodes[0]->duration);
	}
}