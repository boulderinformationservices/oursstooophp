<?php
namespace OuRssToOoPhp;

use UnexpectedValueException;

require_once (__DIR__ . '/Episode.php');
require_once (__DIR__ . '/Podcast.php');

/**
 * Class OuRssToOoPhp
 * @package OuRssToOoPhp
 * @property \DOMDocument $doc
 * @property Podcast $podcast
 * @property \DOMXPath $xpath
 */
class OuRssToOoPhp
{
	private $doc;
	private $podcast;
	private $xpath;

	public function __construct(string $urlToRss)
	{
		$this->doc = new \DOMDocument();
		$this->doc->load($urlToRss);
		$this->xpath = new \DOMXPath($this->doc);
		$this->xpath->registerNamespace("itunes", "http://www.itunes.com/dtds/podcast-1.0.dtd");

		$this->podcast = new Podcast();
		$this->podcast->url = $urlToRss;
	}

	public function testEchoDoc()
	{
		return $this->doc->saveXML();
	}

	public function getPodcast() : Podcast
	{
		$this->podcast->title = $this->getSingleNode(Podcast::getXpath('title'));
		$this->podcast->link = $this->getSingleNode(Podcast::getXpath('link'));
		$this->podcast->description = $this->getSingleNode(Podcast::getXpath('description'));
		$this->podcast->language = $this->getSingleNode(Podcast::getXpath('language'));
		$this->podcast->subtitle = $this->getSingleNode(Podcast::getXpath('subtitle'));
		$this->podcast->summary = $this->getSingleNode(Podcast::getXpath('summary'));
		$this->podcast->author = $this->getSingleNode(Podcast::getXpath('author'));
		$this->podcast->owner = $this->getSingleNode(Podcast::getXpath('owner'));
		$this->podcast->image = $this->getSingleNode(Podcast::getXpath('image'));
		try {
			$this->podcast->keywords = $this->getSingleNode(Podcast::getXpath('keywords'));
		} catch (UnexpectedValueException $e) {
			$this->podcast->keywords = null;
		}
		$this->podcast->categories = $this->getManyNodesAsArray(Podcast::getXpath('categories'));

		return $this->podcast;
	}

	public function getEpisodes() : array
	{
		$episodeNodes = $this->xpath->query('//channel/item');
		$this->podcast->episodes = $this->makeEpisodesFromNodes($episodeNodes);
		return $this->podcast->episodes;
	}

	private function makeEpisodesFromNodes(\DOMNodeList $episodeNodes) : array
	{
		$episodes = [];
		foreach ($episodeNodes as $node)
		{
			$episode = new Episode();
			$episode->title = $this->getSingleNode(Episode::getRelativeXpathFromItem('title'), $node);
			$episode->description = $this->getSingleNode(Episode::getRelativeXpathFromItem('description'), $node);
			$episode->link = $this->getSingleNode(Episode::getRelativeXpathFromItem('link'), $node);
			$episode->pubDate = $this->getSingleNode(Episode::getRelativeXpathFromItem('pubDate'), $node);
			$episode->enclosure_url = $this->getSingleNode(Episode::getRelativeXpathFromItem('enclosure_url'), $node);
			$episode->enclosure_type = $this->getSingleNode(Episode::getRelativeXpathFromItem('enclosure_type'), $node);
			$episode->enclosure_length = $this->getSingleNode(Episode::getRelativeXpathFromItem('enclosure_length'), $node);
			$episode->duration = $this->getOptionalNode(Episode::getRelativeXpathFromItem('duration'), $node);
			$episode->explicit = $this->getSingleNode(Episode::getRelativeXpathFromItem('explicit'), $node);
			$episodes[] = $episode;
		}
		return $episodes;
	}

	private function getOptionalNode(string $path, \DOMNode $contextnode = null) : ?string
	{
		$nodes = $this->xpath->query($path, $contextnode);
		if (count($nodes) > 1)
		{
			$count = count($nodes);
			throw new UnexpectedValueException("Xpath $path was expected to return 0 or 1 nodes. Returned $count instead.");
		}
		if (count($nodes) === 0)
		{
			return null;
		}
		return $nodes[0]->nodeValue;
	}

	private function getSingleNode(string $path, \DOMNode $contextnode = null) : string
	{
		$nodes = $this->xpath->query($path, $contextnode);
		if (count($nodes) !== 1)
		{
			$count = count($nodes);
			throw new UnexpectedValueException("Xpath $path was expected to return exactly 1 nodes. Returned $count instead.");
		}
		return $nodes[0]->nodeValue;
	}

	private function getManyNodesAsArray(string $path) : array
	{
		$values = [];
		$nodes = $this->xpath->query($path);
		foreach ($nodes as $node)
		{
			$values[] = $node->nodeValue;
		}
		return $values;
	}
}