<?php


namespace OuRssToOoPhp;


class Podcast
{
	public $url;
	public $title;
	public $link;
	public $description;
	public $language;
	public $subtitle;
	public $summary;
	public $author;
	public $owner;
	public $image;
	public $keywords;
	public $categories;
	public $episodes;

	public static function getXpath(string $propertyName): string
	{
		$propertyToXpath = [
			'title' => '//channel/title',
			'link' => '//channel/link',
			'description' => '//channel/description',
			'language' => '//channel/language',
			'subtitle' => '//channel/itunes:subtitle',
			'summary' => '//channel/itunes:summary',
			'author' => '//channel/itunes:author',
			'owner' => '//channel/itunes:owner',
			'image' => '//channel/itunes:image/@href',
			'keywords' => '//channel/itunes:keywords',
			'categories' => '//channel/itunes:category/@text',
		];
		return $propertyToXpath[$propertyName];
	}
}