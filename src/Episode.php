<?php


namespace OuRssToOoPhp;


class Episode
{
	public $title;
	public $description;
	public $link;
	public $pubDate;
	public $enclosure_url;
	public $enclosure_type;
	public $enclosure_length;
	public $duration;
	public $explicit;

	public static function getRelativeXpathFromItem(string $propertyName): string
	{
		$propertyToXpathFromItem = [
			'title' => './title',
			'description' => './description',
			'link' => './link',
			'pubDate' => './pubDate',
			'enclosure_url' => './enclosure/@url',
			'enclosure_type' => './enclosure/@type',
			'enclosure_length' => './enclosure/@length',
			'duration'=> './itunes:duration',
			'explicit' => './itunes:explicit',
		];
		return $propertyToXpathFromItem[$propertyName];
	}
}